import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export interface User {
  id: number;
  name: string;
  email: string;
  phone: string;
  age: number;
  gender: string;
  photo: string;
  address: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  userslist: any;
  userform: FormGroup;

  constructor(private http: HttpClient, private route: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getUserList();
    this.userform = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern('^[789]\\d{9}$')]],
      age: ['', [Validators.required, Validators.min(0), Validators.max(100)]],
      gender: ['', Validators.required],
      photo: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  getUserList(): void {
    this.http.get('../../assets/users.json').subscribe(userList => {
      this.userslist = userList;
    });
  }

  addUser(): void {
    console.log(this.userform.getRawValue());
    if (!this.userform.getRawValue().id) {
      this.userform.get('id').setValue(this.userslist.length + 1);
      this.userslist.push(this.userform.getRawValue());
    } else if (this.userform.getRawValue().id && this.userform.getRawValue().id > 0) {
      const userToUpdate = this.userslist.find(element => element.id === this.userform.getRawValue().id);
      this.userslist[this.userslist.indexOf(userToUpdate)] = this.userform.getRawValue();
    }
    this.userform.reset();
  }

  deleteRow(row: any): void {
    const index = this.userslist.indexOf(row);
    this.userslist.splice(index, 1);
  }

  updateRow(user: User): void {
    this.userform = this.formBuilder.group({
      id: [user.id],
      name: [user.name, Validators.required],
      email: [user.email, [Validators.required, Validators.email]],
      phone: [user.phone, [Validators.required, Validators.pattern('^[789]\\d{9}$')]],
      age: [user.age, [Validators.required, Validators.min(0), Validators.max(100)]],
      gender: [user.gender, Validators.required],
      photo: [user.photo, Validators.required],
      address: [user.address, Validators.required]
    });
  }

  readUrl(event: any): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: ProgressEvent) => {
        this.userform.get('photo').setValue((<FileReader>event.target).result);
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }
}
